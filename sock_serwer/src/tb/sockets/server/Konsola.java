package tb.sockets.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Konsola {
    private static ServerSocket serverSocket;
    private static Socket socket;
    private static BufferedReader bufferedReader;


    public static void main(String[] args) throws IOException {
        int[] arrays = new int[9];
        try {
            serverSocket = new ServerSocket(6666);
            socket = serverSocket.accept();

            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String data;
            while ((data = bufferedReader.readLine()) != null) {
                String string = data.toString();
                switch ((int)string.charAt(0)) {
                    case '0':
                        arrays[0]++;
                        break;
                    case '1':
                        arrays[1]++;
                        break;
                    case '2':
                        arrays[2]++;
                        break;
                    case '3':
                        arrays[3]++;
                        break;
                    case '4':
                        arrays[4]++;
                        break;
                    case '5':
                        arrays[5]++;
                        break;
                    case '6':
                        arrays[6]++;
                        break;
                    case '7':
                        arrays[7]++;
                        break;
                    case '8':
                        arrays[8]++;
                        break;

                }

            }

            for (int i = 0; i < 9; i++){
                System.out.println("Button number " + i  + " was " +arrays[i] + " pressed.");;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bufferedReader.close();
            socket.close();
            serverSocket.close();
        }
    }

}
