package tb.sockets.client;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class OrderPane extends JPanel {

    /**
     * Create the panel.
     */
    public OrderPane(Client client) {
        createGUI(client);
    }

    public void createGUI(Client client) {
        this.setLayout(new GridLayout(3, 3));

        JButton btn1x1 = new JButton();
        btn1x1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(0);
                if (btn1x1.getText().equals("")) {
                    client.incrementPressed(0);
                    try {
                        client.getOutputStream().writeBytes("0" + client.getPressed(0) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn1x1.setText("X");
                } else {
                    client.incrementPressed(0);
                    try {
                        client.getOutputStream().writeBytes("0" + client.getPressed(0) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn1x1.setText("");
                }
                btn1x1.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn1x1);


        JButton btn1x2 = new JButton();
        btn1x2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(1);
                if (btn1x2.getText().equals("")) {
                    client.incrementPressed(1);
                    try {
                        client.getOutputStream().writeBytes("1" + client.getPressed(1) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn1x2.setText("X");
                } else {
                    client.incrementPressed(1);
                    try {
                        client.getOutputStream().writeBytes("1" + client.getPressed(1) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn1x2.setText("");
                }
                btn1x2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn1x2);


        JButton btn1x3 = new JButton();
        btn1x3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(2);

                if (btn1x3.getText().equals("")) {
                    client.incrementPressed(2);
                    try {
                        client.getOutputStream().writeBytes("2" + client.getPressed(2) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn1x3.setText("X");
                } else {
                    client.incrementPressed(2);
                    try {
                        client.getOutputStream().writeBytes("2" + client.getPressed(2) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn1x3.setText("");
                }
                btn1x3.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn1x3);


        JButton btn2x1 = new JButton();
        btn2x1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(3);
                if (btn2x1.getText().equals("")) {
                    client.incrementPressed(3);
                    try {
                        client.getOutputStream().writeBytes("3" + client.getPressed(3) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn2x1.setText("X");
                } else {
                    client.incrementPressed(3);
                    try {
                        client.getOutputStream().writeBytes("3" + client.getPressed(3) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn2x1.setText("");
                }
                btn2x1.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn2x1);


        JButton btn2x2 = new JButton();
        btn2x2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(4);
                if (btn2x2.getText().equals("")) {
                    client.incrementPressed(4);
                    try {
                        client.getOutputStream().writeBytes("4" + client.getPressed(4) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn2x2.setText("X");
                } else {
                    client.incrementPressed(4);
                    try {
                        client.getOutputStream().writeBytes("4" + client.getPressed(4) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn2x2.setText("");
                }
                btn2x2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn2x2);


        JButton btn2x3 = new JButton();
        btn2x3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(5);
                if (btn2x3.getText().equals("")) {
                    client.incrementPressed(5);
                    try {
                        client.getOutputStream().writeBytes("5" + client.getPressed(5) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn2x3.setText("X");
                } else {
                    client.incrementPressed(5);
                    try {
                        client.getOutputStream().writeBytes("5" + client.getPressed(5) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn2x3.setText("");
                }
                btn2x3.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn2x3);


        JButton btn3x1 = new JButton();
        btn3x1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(6);
                if (btn3x1.getText().equals("")) {
                    client.incrementPressed(6);
                    try {
                        client.getOutputStream().writeBytes("6" + client.getPressed(6) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn3x1.setText("X");
                } else {
                    client.incrementPressed(6);
                    try {
                        client.getOutputStream().writeBytes("6" + client.getPressed(6) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn3x1.setText("");
                }
                btn3x1.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn3x1);


        JButton btn3x2 = new JButton();
        btn3x2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(7);
                if (btn3x2.getText().equals("")) {
                    client.incrementPressed(7);
                    try {
                        client.getOutputStream().writeBytes("7" + client.getPressed(7) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn3x2.setText("X");
                } else {
                    client.incrementPressed(7);
                    try {
                        client.getOutputStream().writeBytes("7" + client.getPressed(7) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn3x2.setText("");
                }
                btn3x2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn3x2);


        JButton btn3x3 = new JButton();
        btn3x3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.getPlayer().incrementPressedButtons(8);
                if (btn3x3.getText().equals("")) {
                    client.incrementPressed(8);
                    try {
                        client.getOutputStream().writeBytes("8" + client.getPressed(8) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn3x3.setText("X");
                } else {
                    client.incrementPressed(8);
                    try {
                        client.getOutputStream().writeBytes("8" + client.getPressed(8) + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btn3x3.setText("");
                }
                btn3x3.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
            }
        });
        this.add(btn3x3);


    }
}
