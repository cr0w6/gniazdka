package tb.sockets.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {
    private DataOutputStream outputStream;
    private Socket socket;
    private Player player;
    private int[] pressed;

    public Client() {
        try {
            player = new Player();
            socket = null;
            outputStream = null;
            pressed = new int[9];
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) throws IOException {
        this.socket = socket;
        outputStream = new DataOutputStream(this.socket.getOutputStream());
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(DataOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void close(){
        try {
            socket.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void incrementPressed(int i){
        pressed[i]++;
    }

    public int getPressed(int i) {
        return pressed[i];
    }
}
