package tb.sockets.client;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Player {
    private static int id = 0;
    private int[] pressedButtons;

    public Player() {
        pressedButtons = new int[9];
        id++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void incrementPressedButtons(int i){
        pressedButtons[i]++;
    }

    public int getPressedButtons(int i) {
        return pressedButtons[i];
    }
}
