package tb.sockets.client;

import java.awt.event.*;
import java.io.IOException;
import java.net.Socket;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import java.awt.Color;

public class MainFrame extends JFrame {

    private JPanel contentPane;
    private JFormattedTextField hostTextField;
    private JFormattedTextField portTextField;

    public MainFrame(Client client) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 650, 500);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblHost = new JLabel("Host:");
        lblHost.setBounds(10, 15, 50, 20);
        contentPane.add(lblHost);

        hostTextField = new JFormattedTextField();
        try {
            hostTextField = new JFormattedTextField(new MaskFormatter("###.#.#.#"));
            hostTextField.setBounds(55, 15, 90, 20);
            contentPane.add(hostTextField);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JLabel lblPort = new JLabel("Port:");
        lblPort.setBounds(10, 45, 50, 20);
        contentPane.add(lblPort);

        portTextField = new JFormattedTextField();
        portTextField.setBounds(55, 45, 90, 20);
        contentPane.add(portTextField);


        OrderPane board = new OrderPane(client);
        board.setBounds(150, 14, 487, 448);
        contentPane.add(board);
        board.setVisible(false);


        JLabel lblNotConnected = new JLabel("Not Connected");
        lblNotConnected.setForeground(new Color(255, 255, 255));
        lblNotConnected.setBackground(new Color(128, 128, 128));
        lblNotConnected.setOpaque(true);
        lblNotConnected.setBounds(10, 104, 135, 23);
        contentPane.add(lblNotConnected);

        JButton btnConnect = new JButton("Connect");
        btnConnect.setBounds(10, 70, 135, 23);
        contentPane.add(btnConnect);
        btnConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String hostDate = hostTextField.getText();
                int portDate = Integer.parseInt(portTextField.getText());

                try {
                    client.setSocket(new Socket(hostDate, portDate));

                    board.setVisible(true);
                    lblNotConnected.setText("Connect");
                    lblNotConnected.setForeground(Color.BLACK);
                    lblNotConnected.setBackground(Color.GREEN);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
}
